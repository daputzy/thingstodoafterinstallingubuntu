### Mouse Acceleration Fix

[More](http://www.techytalk.info/turn-mouse-acceleration-off-on-ubuntu-linux/comment-page-1/)

    # find device id
    xinput list

Add to file:
    
    nano ~/.profile
    
Content:

    if [[ $(xinput list | grep 'ZOWIE' | wc -l) -eq 1 ]]; then
        # acceleration
        xinput set-prop 'Kingsis Peripherals ZOWIE Gaming mouse' 'Device Accel Profile' -1
        # speed
        xinput set-prop 'Kingsis Peripherals ZOWIE Gaming mouse' 'Device Accel Constant Deceleration' 1.5
    fi

### Pidgin

    sudo add-apt-repository ppa:nilarimogard/webupd8 &&
    sudo apt-get update &&
    sudo apt-get install pidgin

### Skype

[Downloadlink](http://www.skype.com/en/download-skype/skype-for-computer/)

    sudo apt-get install sni-qt sni-qt:i386 gtk2-engines-murrine:i386 gtk2-engines-pixbuf:i386 libasound2-plugins:i386
    
    sudo dpkg -i skype*.deb
        
    # if needed:
    sudo apt-get -f install

### Chrome

    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - &&
    sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' &&
    sudo apt-get update &&
    sudo apt-get install google-chrome-stable
    
> remove doubled sources in /etc/apt/sources.list.d/

### Java

    sudo add-apt-repository ppa:webupd8team/java &&
    sudo apt-get update &&
    sudo apt-get install oracle-java8-installer &&
    sudo apt-get install oracle-java8-set-default
    
### Dropbox

    # mate and gnome
    sudo apt-get install nautilus-dropbox

    # unity
    sudo apt-get install dropbox

### Keepass

    sudo add-apt-repository ppa:eugenesan/ppa &&
    sudo apt-get update &&
    sudo apt-get install keepassx
    
### More software

    # php
    sudo apt-get install php7.0-fpm php7.0-cli php7.0-curl php7.0-xml

    # bzr
    sudo apt-get install bzr bzr-doc bzrtools

    # stuff
    sudo apt-get install git vlc tree htop gparted gedit curl synaptic

    # more stuff
    sudo apt-get install sublime-text playonlinux filezilla meld mysql-client

### Composer

    # download
    curl -sS https://getcomposer.org/installer | php

    # "install"
    sudo mv composer.phar /usr/bin/composer

### MATE - Settings

    sudo apt-get install indicator-multiload nautilus-open-terminal

    # fix default terminal
    gsettings set org.gnome.desktop.default-applications.terminal exec 'gnome-terminal'

    # start indicator-multiload
    indicator-multiload

### GNOME - Gnome Shell Extension

    sudo add-apt-repository ppa:ne0sight/chrome-gnome-shell &&
    sudo apt-get update &&
    sudo apt-get install chrome-gnome-shell

* [https://extensions.gnome.org/extension/495/topicons/](https://extensions.gnome.org/extension/495/topicons/)
* [https://extensions.gnome.org/extension/118/no-topleft-hot-corner/](https://extensions.gnome.org/extension/118/no-topleft-hot-corner/)
* [https://extensions.gnome.org/extension/307/dash-to-dock/](https://extensions.gnome.org/extension/307/dash-to-dock/)
* [https://extensions.gnome.org/extension/120/system-monitor/](https://extensions.gnome.org/extension/120/system-monitor/)

### GNOME - Apport

Disable Apport:

    sudo gedit /etc/default/apport

### UNITY - Unity Tweak Tools

    sudo apt-get install unity-tweak-tool compizconfig-settings-manager dconf-editor

    # no ads
    gsettings set com.canonical.Unity.Lenses disabled-scopes "['more_suggestions-amazon.scope', 'more_suggestions-u1ms.scope', 'more_suggestions-populartracks.scope', 'music-musicstore.scope', 'more_suggestions-ebay.scope', 'more_suggestions-ubuntushop.scope', 'more_suggestions-skimlinks.scope']"
    
> Then, open CompizConfig Settings Manager from Dash, click the "Ubuntu Unity Plugin" and on the Launcher tab, enable "Minimize single window applications (unsupported)".

> Since the Unity global menu is not appreciated by everybody, maybe you'll find LIM better, so give it a try. To enable it, open System Settings > Appearance and on the Behavior tab, select to show the menus "in the window's title bar".

> For now, LIM continues to use autohide and there's no option in the System Settings to enable the recently introduced "always show menus" option so if you want to enable that option, you'll have to use Dconf Editor (com > canonical > unity and enable "always-show-menus").

> On the Search tab, you can select to disable online search results from being displayed in Dash. However, you may want to use some scopes so instead of completely disabling this, you can open Dash and on the applications lens (the second lens) click "Filter results", then select "Dash plugins" - here, you can enable/disable any Dash plugin you want
    
### Fonts

    sudo apt-get install console-terminus ttf-dejavu fonts-droid fonts-inconsolata xfonts-terminus &&
    sudo apt-get install ttf-liberation ttf-ubuntu-font-family ttf-xfree86-nonfree

    sudo fc-cache -fv

    sudo add-apt-repository ppa:fontforge/fontforge &&
    sudo apt-get update &&
    sudo apt-get install fontforge font-manager

* [Install Consolas](https://www.rushis.com/consolas-font-on-ubuntu/)
* [Remove Hinting - 1](http://planet.jboss.org/post/how_to_disable_font_hinting_in_swing_or_how_to_strip_hints_from_a_font)
* [Remove Hinting - 2](https://stackoverflow.com/questions/17508/how-to-modify-the-style-property-of-a-font-on-windows)

### CleanUp

    sudo apt-get update &&
    sudo apt-get upgrade &&
    sudo apt-get -f install &&
    sudo apt-get autoremove &&
    sudo apt-get -y autoclean &&
    sudo apt-get -y clean

### Fixing Font Rendering in jetbrains-products

add to `bin/*64.vmoptions`:

    -Dswing.aatext=true
    -Dawt.useSystemAAFontSettings=gasp
    -Dsun.java2d.xrender=true

or `-Dawt.useSystemAAFontSettings=lcd`

### Netbeans

     --laf Windows -J-Dawt.useSystemAAFontSettings=on -J-Dswing.aatext=true

* [http://wiki.netbeans.org/NBLookAndFeels](http://wiki.netbeans.org/NBLookAndFeels)
* [http://askubuntu.com/questions/231711/font-rendering-in-gedit-is-smooth-but-not-smooth-in-netbeans](http://askubuntu.com/questions/231711/font-rendering-in-gedit-is-smooth-but-not-smooth-in-netbeans)

### More

* [http://www.webupd8.org/2014/04/10-things-to-do-after-installing-ubuntu.html](http://www.webupd8.org/2014/04/10-things-to-do-after-installing-ubuntu.html)
* [http://scienceblogs.com/gregladen/2014/04/24/10-or-20-things-to-do-after-installing-ubuntu-14-04-trusty-tahr/](http://scienceblogs.com/gregladen/2014/04/24/10-or-20-things-to-do-after-installing-ubuntu-14-04-trusty-tahr/)
* [http://ubuntuforums.org/showthread.php?t=1962862](http://ubuntuforums.org/showthread.php?t=1962862)
* [http://www.webupd8.org/2015/09/terminal-emulator-terminator-sees-new.html?m=1](http://www.webupd8.org/2015/09/terminal-emulator-terminator-sees-new.html?m=1)
* [http://www.noobslab.com/2012/04/important-things-to-do-after-install_26.html?m=1](http://www.noobslab.com/2012/04/important-things-to-do-after-install_26.html?m=1)
* [http://www.erikaheidi.com/blog/setting-up-a-development-machine-with-ubuntu-1404-trusty-tahr](http://www.erikaheidi.com/blog/setting-up-a-development-machine-with-ubuntu-1404-trusty-tahr)
* [http://www.techrepublic.com/blog/linux-and-open-source/six-must-have-ubuntu-unity-tweaks/](http://www.techrepublic.com/blog/linux-and-open-source/six-must-have-ubuntu-unity-tweaks/)
* [http://www.techdrivein.com/2012/06/25-things-i-did-after-installing-ubuntu.html](http://www.techdrivein.com/2012/06/25-things-i-did-after-installing-ubuntu.html)

### List to sight
- [http://community.linuxmint.com/tutorial/view/365](http://community.linuxmint.com/tutorial/view/365)
- [http://catlingmindswipe.blogspot.de/2011/12/how-to-install-microsoft-windows-fonts.html](http://catlingmindswipe.blogspot.de/2011/12/how-to-install-microsoft-windows-fonts.html)
- [http://www.johndcook.com/blog/2009/09/21/free-alternative-to-consolas-font/](http://www.johndcook.com/blog/2009/09/21/free-alternative-to-consolas-font/)
- [http://www.pcworld.com/article/2863497/how-to-install-microsoft-fonts-in-linux-office-suites.html](http://www.pcworld.com/article/2863497/how-to-install-microsoft-fonts-in-linux-office-suites.html)